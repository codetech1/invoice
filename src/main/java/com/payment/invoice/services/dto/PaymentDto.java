package com.payment.invoice.services.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "payment")
public class PaymentDto {

	@Id
	@GeneratedValue(generator = "payment_generator")
	@SequenceGenerator(name = "payment_generator", sequenceName = "payment_row_id_seq", initialValue = 1000)
	@Column(name = "row_id")
    private Long id;
	
	@NotBlank
	@Column(name = "code",unique = true)
    private String code;
	
	@Size(max = 250)
    private String description;
	
	@Column(name = "creation_date")
    private String creationDate;
	
	@Column(name = "days")
    private Integer days;
	
	@Column(name = "remind_before_days")
    private Integer remindBeforeDays;
	
}
