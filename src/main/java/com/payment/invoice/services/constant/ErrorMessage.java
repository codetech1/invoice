package com.payment.invoice.services.constant;

public class ErrorMessage {

	public static final String RECORD_NOT_FOUND = "Page or record could not found, please check request filters or url";
	
}
