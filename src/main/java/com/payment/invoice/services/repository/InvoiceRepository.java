package com.payment.invoice.services.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.payment.invoice.services.dto.InvoiceDto;

@Repository
public interface InvoiceRepository extends JpaRepository<InvoiceDto, String>{

	/**
	 * All unpaid invoices will get from database by using unpaid filter.
	 * 
	 * @param unpaid
	 * @return
	 */
	List<InvoiceDto> findByStatus(String unpaid);

}
