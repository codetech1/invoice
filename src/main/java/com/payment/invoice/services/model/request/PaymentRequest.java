package com.payment.invoice.services.model.request;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentRequest {

	private Long id;

	@NotBlank(message = "Please provide a code")
	private String code;

	@Size(max = 250, message="Please reduce description data below 250 chars")
	private String description;

	@NotBlank(message = "Please provide a code")
	@Pattern(message = "Please enter date in valid format yyyy-mm-dd", regexp = "^[0-9]{4}-(1[0-2]|0[1-9])-(3[01]|[12][0-9]|0[1-9])$")
	private String creationDate;

	@NotNull(message = "Please provide days")
	private Integer days;

	@NotNull(message = "Please provide remindBeforeDays")
	private Integer remindBeforeDays;
}
