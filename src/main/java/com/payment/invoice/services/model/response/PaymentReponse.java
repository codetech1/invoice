package com.payment.invoice.services.model.response;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentReponse {
	

	private Long id;	
	
    private String code;
	
    private String description;
	
    private String creationDate;
	
    private Integer days;
    
    private Integer remindBeforeDays;
}
