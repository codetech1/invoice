package com.payment.invoice.services.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

public class Util {

	private static ModelMapper mapper = new ModelMapper();

	public static <T> T convert(Object source, Class<T> destination) {
		if (source == null) {
			return null;
		}
		return mapper.map(source, destination);
	}

	public static String getControllerUrlFromWebRequest(WebRequest request) {
		String prepareURl = ((ServletWebRequest) request).getRequest().getRequestURI().toString();
		return prepareURl.replace(request.getContextPath(), "");
	}
	
	public static Date addDays(String formatedDate, Integer addDaysCount) {
		//Specifying date format that matches the given date
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Calendar c = Calendar.getInstance();
		try {
			//Setting the date to the given date
			c.setTime(sdf.parse(formatedDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		//Number of Days to add
		c.add(Calendar.DAY_OF_MONTH, addDaysCount);
		return c.getTime();
	}
	
	public static Boolean compareTwoDates(Date startDate, Date endDate) {
		Date sDate = getZeroTimeDate(startDate);
		Date eDate = getZeroTimeDate(endDate);

		if (eDate.before(sDate)) {
			return true;
		}
		if (sDate.equals(eDate)) {
			return true;
		}
		return false;
	}
	
	public static Date getZeroTimeDate(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		date = calendar.getTime();
		return date;
	}
	
}
