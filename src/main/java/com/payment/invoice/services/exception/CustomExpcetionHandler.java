package com.payment.invoice.services.exception;


import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.payment.invoice.services.model.ErrorResponse;
import com.payment.invoice.services.utils.Util;

@ControllerAdvice
public class CustomExpcetionHandler extends ResponseEntityExceptionHandler {

    private static final Logger logger = LogManager.getLogger(CustomExpcetionHandler.class);
    
    @ExceptionHandler(RecordNotFoundException.class)
    public final ResponseEntity<Object> handleRecordNotFoundException(RecordNotFoundException ex, WebRequest request) {
        String details = ex.getLocalizedMessage();
        ErrorResponse errorresponse = new ErrorResponse(404,
                Util.getControllerUrlFromWebRequest(request), "Record Not Found", details);
        logger.error("Record Not Found", errorresponse);
        return new ResponseEntity<Object>(errorresponse, HttpStatus.NOT_FOUND);
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {
        // Get all errors
        List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        ErrorResponse errorresponse = new ErrorResponse(400,
                Util.getControllerUrlFromWebRequest(request), "Validation Failed", errors);
        return new ResponseEntity<Object>(errorresponse, headers, status);
    }
   

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> exceptionHandler(Exception ex, WebRequest request) {
        String details = ex.getLocalizedMessage();
        ErrorResponse errorresponse = new ErrorResponse(500,
                Util.getControllerUrlFromWebRequest(request), "Server Error", details);        
        return new ResponseEntity<Object>(errorresponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
