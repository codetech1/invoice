package com.payment.invoice.services.service.api;

import java.util.List;

import com.payment.invoice.services.model.response.InvoiceResponse;

public interface InvoiceService {

	List<InvoiceResponse> getAllUnPaidInvoices();
}
