package com.payment.invoice.services.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.payment.invoice.services.constant.ErrorMessage;
import com.payment.invoice.services.dto.PaymentDto;
import com.payment.invoice.services.exception.RecordNotFoundException;
import com.payment.invoice.services.model.request.PaymentRequest;
import com.payment.invoice.services.model.response.InvoiceResponse;
import com.payment.invoice.services.model.response.PaymentReponse;
import com.payment.invoice.services.repository.PaymentRepository;
import com.payment.invoice.services.service.api.PaymentService;
import com.payment.invoice.services.utils.Util;

@Service
public class PaymentServiceImpl implements PaymentService{

	@Autowired
	PaymentRepository paymentRepository;

	/**
	 * Method is used for save the record in database using PaymentRequest object
	 * 
	 * @param request
	 * @return PaymentReponse
	 */
	@Override
	public PaymentReponse save(PaymentRequest request) {
		PaymentDto requestPayment = Util.convert(request, PaymentDto.class);
		if(requestPayment.getId() != null) {
			validate(requestPayment.getId());
		}
		return Util.convert(paymentRepository.save(requestPayment), PaymentReponse.class);
	}
	
	private void validate(Long id) {
		Optional<PaymentDto> fetchedPayment = paymentRepository.findById(id);
		if(!fetchedPayment.isPresent()) {
			throw new RecordNotFoundException(ErrorMessage.RECORD_NOT_FOUND);
		}
	}

	/**
	 * Method is used for fetch all the records from database
	 * 
	 * @return List<PaymentReponse>
	 */
	@Override
	public List<PaymentReponse> getAll() {
		return paymentRepository.findAll().stream()
				.map(payemntDto->{return Util.convert(payemntDto, PaymentReponse.class);})
				.collect(Collectors.toList());
	}

	/**
	 * Method is used for fetch record by ID from database
	 * 
	 * @param paymentID
	 * @return PaymentReponse
	 */
	@Override
	public PaymentReponse findById(Long paymentID) {
		Optional<PaymentDto> fetchedPayment = paymentRepository.findById(paymentID);
		if(!fetchedPayment.isPresent()) {
			throw new RecordNotFoundException(ErrorMessage.RECORD_NOT_FOUND);
		}
		return Util.convert(fetchedPayment.get(), PaymentReponse.class);
	}

	/**
	 * Method is used for delete record by ID from database
	 * 
	 * @param paymentID
	 */
	@Override
	public void delete(Long paymentID) {
		validate(paymentID);
		paymentRepository.deleteById(paymentID);
	}

	/**
	 * Method is used to check the date is in reminder range or not.
	 * 
	 * @param unpaidInvoice
	 * @return
	 */
	@Override
	public Boolean checkAndvalidateDateForReminder(InvoiceResponse unpaidInvoice) {
		PaymentDto fetchedPayment =  paymentRepository.findByCode(unpaidInvoice.getPaymentTerm());
		Date reminderDate = Util.addDays(unpaidInvoice.getInvoiceDate(), fetchedPayment.getDays() - fetchedPayment.getRemindBeforeDays());
		return Util.compareTwoDates(new Date(), reminderDate);
	}
	
	
}
