package com.payment.invoice.services.enums;

import org.springframework.http.HttpStatus;

public enum ResponseEnum {
	
	OK(200,HttpStatus.OK.name()),
	CREATED(201,HttpStatus.CREATED.name()),
	UPDATED(202,"UPDATED"),
	DELETED(203,"DELETED"),
	NO_CONTENT(204,HttpStatus.NO_CONTENT.name()),
	BAD_REQUEST(400,HttpStatus.BAD_REQUEST.name()),		
	NOTFOUND(404,HttpStatus.NOT_FOUND.name()),
	INTERNALSERVERERROR(500,HttpStatus.INTERNAL_SERVER_ERROR.name());
	

	private final Integer statusCode;
    private final String statusMessage;
    
    private ResponseEnum(Integer statusCode, String statusMessage) {
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }
    
    public Integer getCode() {
        return this.statusCode;
    }

    public String getMessage() {
        return this.statusMessage;
    }
    
    public static ResponseEnum getResponse(int code) {
    	for(ResponseEnum re : ResponseEnum.values()) {
    		if(re.statusCode==code) {
    			return re;
    		}
    	}
    	return null;
    }
   
 }
