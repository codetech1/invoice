package com.payment.invoice.services.contoller;

import java.util.List;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.payment.invoice.services.model.ResponseBean;
import com.payment.invoice.services.model.request.PaymentRequest;
import com.payment.invoice.services.model.response.PaymentReponse;
import com.payment.invoice.services.service.api.PaymentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = ("PaymentController"))
@RestController
@RequestMapping("/payments")
public class PaymentController {

	
	private static final Logger logger = LogManager.getLogger(PaymentController.class);
	
	
	@Autowired
	PaymentService paymentService;
	
	
	/**
	 * Method is used for save the record in database using PaymentRequest object
	 * 
	 * @param request
	 * @return PaymentReponse
	 */
	@ApiOperation(value = "Save Payments", notes = "Method is used for save the record in database using PaymentRequest object")
	@PostMapping
    public ResponseEntity<ResponseBean> savePayments(@RequestBody @Valid PaymentRequest request) {
		logger.info("Started into {} savePayments", "savePayments");
		PaymentReponse response = paymentService.save(request);
		logger.info("End into {} savePayments",response);
		return new ResponseEntity<ResponseBean>(new ResponseBean(201, "/payments", response),HttpStatus.CREATED);
    }
	

	/**
	 * Method is used for fetch all the records from database
	 * 
	 * @return List<PaymentReponse>
	 */
	@ApiOperation(value = "Get All Payments", notes = "Method is used for get all the records from database")
	@GetMapping
    public ResponseEntity<ResponseBean> getPayments() {
		logger.info("Started into {} getPayments", "getPayments");
		List<PaymentReponse> responses = paymentService.getAll();
		logger.info("End into {} getPayments",responses);
		return new ResponseEntity<ResponseBean>(new ResponseBean(200, "/payments", responses),HttpStatus.OK);
    }
	
	
	/**
	 * Method is used for fetch record by ID from database
	 * 
	 * @param paymentID
	 * @return PaymentReponse
	 */
	@ApiOperation(value = "Get All Payments", notes = "Method is used for get single record from database by ID")
	@GetMapping("/{paymentID}")
    public ResponseEntity<ResponseBean> getPaymentbyID(@PathVariable("paymentID") Long paymentID ) {
		logger.info("Started into {} getPaymentbyID", "getPaymentbyID");
		PaymentReponse responses = paymentService.findById(paymentID);
		logger.info("End into {} getPaymentbyID",responses);
		return new ResponseEntity<ResponseBean>(new ResponseBean(200, "/payments/"+paymentID, responses),HttpStatus.OK);
    }
	
	/**
	 * Method is used for delete record by ID from database
	 * 
	 * @param paymentID
	 */
	@ApiOperation(value = "Delete Payment", notes = "Method is used for delete single record from database by ID")
	@DeleteMapping("/{paymentID}")
    public ResponseEntity<ResponseBean> deletePaymentbyID(@PathVariable("paymentID") Long paymentID ) {
		logger.info("Started into {} deletePaymentbyID", "getPaymentbyID");
		paymentService.delete(paymentID);
		logger.info("End into {} deletePaymentbyID");
		return new ResponseEntity<ResponseBean>(new ResponseBean(200, "/payments/"+paymentID, paymentID+" record is successfully deleted."),HttpStatus.OK);
    }
	
	/**
	 * Method is used for update the record in database using PaymentRequest object
	 * 
	 *  @param request
	 *  @return PaymentReponse
	 */
	@ApiOperation(value = "Get All Payments", notes = "Method is used for update the record in database using PaymentRequest object")
	@PutMapping("/{paymentID}")
    public ResponseEntity<ResponseBean> updatePaymentbyID(@PathVariable("paymentID") Long paymentID,
    		@Valid @RequestBody PaymentRequest request ) {
		logger.info("Started into {} getPaymentbyID", "getPaymentbyID");
		request.setId(paymentID);
		PaymentReponse responses = paymentService.save(request);
		logger.info("End into {} getPaymentbyID",responses);
		return new ResponseEntity<ResponseBean>(new ResponseBean(202, "/payments/"+paymentID, responses),HttpStatus.OK);
    }
	
}
