--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE `invoice` (
  `invoice_number` varchar(255) NOT NULL,
  `invoice_date` varchar(255) DEFAULT NULL,
  `payment_term` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`invoice_number`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `invoice`
--

LOCK TABLES `invoice` WRITE;
INSERT INTO `invoice` VALUES ('INV-001','2017-11-10','NET 30','PAID'),('INV-002','2020-12-25','NET 45','UNPAID'),('INV-003','2021-01-15','NET 30','UNPAID'),('INV-004','2021-01-23','NET 15','UNPAID');
UNLOCK TABLES;

--
-- Table structure for table `invoice_number_seq`
--

DROP TABLE IF EXISTS `invoice_number_seq`;
CREATE TABLE `invoice_number_seq` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `invoice_number_seq`
--

LOCK TABLES `invoice_number_seq` WRITE;
INSERT INTO `invoice_number_seq` VALUES (1);
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE `payment` (
  `row_id` bigint NOT NULL,
  `code` varchar(255) DEFAULT NULL,
  `creation_date` varchar(255) DEFAULT NULL,
  `days` int DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `remind_before_days` int DEFAULT NULL,
  PRIMARY KEY (`row_id`),
  UNIQUE KEY `UK_opor0kv54jt58n364jog9bu2i` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
INSERT INTO `payment` VALUES (1201,'NET 30','2017-12-01',30,'Within 30 days',4),(1202,'NET 45','2020-12-25',45,'Within 45 days',5),(1251,'NET 15','2021-01-25',15,'Within 15 days',2);
UNLOCK TABLES;

--
-- Table structure for table `payment_row_id_seq`
--

DROP TABLE IF EXISTS `payment_row_id_seq`;
CREATE TABLE `payment_row_id_seq` (
  `next_val` bigint DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `payment_row_id_seq`
--

LOCK TABLES `payment_row_id_seq` WRITE;
INSERT INTO `payment_row_id_seq` VALUES (1350);
UNLOCK TABLES;